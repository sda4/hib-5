ALTER TABLE klient ADD COLUMN type VARCHAR(30);

UPDATE klient set type='bazowy';

ALTER TABLE klient ADD COLUMN ulubiony_kolor VARCHAR(30);
ALTER TABLE klient ADD COLUMN id_rodzica BIGINT;

ALTER TABLE public.klient
  ADD CONSTRAINT klient_klient_klient_id_fk
FOREIGN KEY (id_rodzica) REFERENCES klient (klient_id);