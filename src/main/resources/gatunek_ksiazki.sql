ALTER TABLE ksiazka DROP CONSTRAINT ksiazka_rodzaj_fkey;

CREATE TABLE gatunek_ksiazki
(
  gatunek_id BIGSERIAL PRIMARY KEY,
  nazwa VARCHAR(100) NOT NULL
);
CREATE UNIQUE INDEX gatunek_ksiazki_nazwa_uindex ON public.gatunek_ksiazki (nazwa);

CREATE TABLE ksiazka_gatunek
(
  id_ksiazki BIGINT NOT NULL,
  id_gatunku BIGINT NOT NULL,
  CONSTRAINT ksiazka_gatunek_gatunek_ksiazki_gatunek_id_fk FOREIGN KEY (id_gatunku) REFERENCES gatunek_ksiazki (gatunek_id),
  CONSTRAINT ksiazka_gatunek_ksiazka_ksiazka_id_fk FOREIGN KEY (id_ksiazki) REFERENCES ksiazka (ksiazka_id)
);