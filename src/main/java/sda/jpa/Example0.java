package sda.jpa;

import sda.jpa.model.Klient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Example0 {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            // TODO: dodac do klasy Klient relacje do klasy Klient
            Klient klient = em.find(Klient.class, 57L);

            while(klient != null) {
                System.out.println(klient.getKlientId());
                klient = klient.getPolecajacy();
            }

            em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }

}
