package sda.jpa;

import sda.jpa.model.*;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Arrays;
import java.util.List;

public class Example1 {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            // TODO: utworzyc tabele gatunek_ksiazki (skrypt SQL w katalogu resources) i zmapowac ja do nowej klasy encji

            // TODO: utworzyc (recznie) w bazie dwa gatunki, nastpnie utworzyc nowa encje Ksiazka,dodac do niej te dwa gatunki
            // i zapisac encje w bazie
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();

            GatunekKsiazki g1 = em.find(GatunekKsiazki.class, 1L);
            GatunekKsiazki g2 = em.find(GatunekKsiazki.class, 2L);

            Ksiazka ksiazka = new Ksiazka();
            ksiazka.setAutor(AutorKsiazki.builder().imieAutora("Ferdynand").nazwiskoAutora("Bach").build());
            ksiazka.setTytul("Elementarz");
            ksiazka.setWiekMinimalnyCzytelnika(1);
            ksiazka.setGatunki(Arrays.asList(g1, g2));

            em.persist(ksiazka);
            em.getTransaction().commit();

            em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
