package sda.jpa.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@DiscriminatorValue("dziecko")
public class KlientNiepelnoletni extends Klient {

    @ManyToOne
    @JoinColumn(name="id_rodzica")
    private Klient rodzic;

    @Column(name="ulubiony_kolor")
    private String ulubionyKolor;
}
