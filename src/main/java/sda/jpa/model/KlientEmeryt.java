package sda.jpa.model;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@DiscriminatorValue("emeryt")
public class KlientEmeryt extends Klient {

    private Integer znizka;
}
