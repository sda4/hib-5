package sda.jpa.model;

import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public class Czlowiek {

    private String imie;

    private String nazwisko;

    @Enumerated(value = EnumType.STRING)
    private Plec plec;
}
