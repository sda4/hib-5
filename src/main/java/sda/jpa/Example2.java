package sda.jpa;

import sda.jpa.model.Klient;
import sda.jpa.model.Ksiazka;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Example2 {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            Ksiazka ksiazka = em.find(Ksiazka.class, 1L);
            Klient klient = em.find(Klient.class, 1L);

            em.close();

            // TODO: utworzyc brakujace relacje zwrotne do recenzji w typach encji Klient i

            em = emf.createEntityManager();

            klient = em.merge(klient);
            ksiazka = em.merge(ksiazka);

            // TODO: naprawic program, zeby poprawnie wyswietlil recenzje dla ksiazki i dla klienta

            klient.getRecenzje().forEach(recenzja -> System.out.println(recenzja.getTrescRecenzji()));
            ksiazka.getRecenzje().forEach(recenzja -> System.out.println(recenzja.getTrescRecenzji()));

            em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
