package sda.jpa;

import sda.jpa.model.Klient;
import sda.jpa.model.KlientEmeryt;
import sda.jpa.model.KlientNiepelnoletni;
import sda.jpa.model.Plec;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Example4 {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            // TODO: dodac do klasy Klient nowa podklase KlientNiepelnoletni, z obowiazkowa relacja do rodzica dokonujacego zapisu
            // do biblioteki


            KlientNiepelnoletni kn = new KlientNiepelnoletni();
            kn.setImie("Stasiu");
            kn.setNazwisko("Jankowski");
            kn.setPlec(Plec.M);
            kn.setRodzic(em.find(Klient.class, 1L));

            em.getTransaction().begin();
            em.persist(kn);
            em.getTransaction().commit();

            Klient klient1 = em.find(Klient.class,68L);
            KlientNiepelnoletni klient2 = em.find(KlientNiepelnoletni.class, 68L); // tu nie znajdzie, bo ten klient nie jest dzieckiem

            Klient klient3 = em.find(Klient.class,69L);
            KlientNiepelnoletni klient4 = em.find(KlientNiepelnoletni.class, 69L); // w obydwu wypadkach znajdzie klienta, bo klient niepelnoletni
            // tez jest klientem (jego specjalistyczna forma)


            KlientEmeryt emeryt = new KlientEmeryt();
            emeryt.setImie("Henryk");
            emeryt.setNazwisko("Jankowski");
            emeryt.setPlec(Plec.M);
            emeryt.setZnizka(5);

            em.getTransaction().begin();
            em.persist(emeryt);
            em.getTransaction().commit();

            em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
