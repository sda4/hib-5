package sda.jpa;

import sda.jpa.model.Klient;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Example3 {
    public static void main(String[] args) {
        EntityManagerFactory emf = null;
        try {
            emf = Persistence.createEntityManagerFactory("sda.jpa.1");
            EntityManager em = emf.createEntityManager();

            // TODO: wyodrebnic pola imie, nazwisko i plec z klasy Klient do nowej klasy Czlowiek
            Klient klient = em.find(Klient.class, 1l);
            System.out.println(klient.getNazwisko());
            em.close();
        } finally {
            if (emf != null) {
                emf.close();
            }
        }
    }
}
